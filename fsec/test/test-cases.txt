
1) Execute dnsfwd without any arguments
2) Execute dnsfwd with garbage characters as parameters
3) Execute dnsfwd with only IP/range
4) Execute dnsfwd with invalid IP / invalid range
5) run dig with non-existing website ( expect to handle duplicate transid and failure response should not be sent to client)
6) run dig in million loop ( observe CPU, memory usage and should not crash )
7) Execute dnsfwd with valgrind to see any memory violation
8) Switch-off internet and then execute dnsfwd. ( should not get stuck in sendto/recvfrom of DNS server )
9) Pass ip/port of some other server(instead of DNS) and make sure not stuck in recvfrom
10) Test dnsfwd with other options supported by "dig"

#!/bin/bash
#Stress test
#Run in a million loop
set -e

touch /tmp/dnsfwd_results

for i in {1..1000000};do
	dig @127.0.0.1 -p 9000 hs.fi 1>/tmp/dnsfwd_results
done

echo "success"

/**
 * Custom exception class which helps in
 * cascading the exceptions neatly with
 * systemcall errno
 */
#ifndef _DNS_FWD_EXCEPTION_H
#define _DNS_FWD_EXCEPTION_H

#include <exception>
#include <string>

namespace dns_fwd
{

class dnsFwdException : public std::exception
{
public:
	explicit dnsFwdException(const char* error,
                      int errCode = 0);
	const char* what() const noexcept;

private:
	void fillStringWithErrno() noexcept;
	std::string errorMsgPtr_;
	int errorCode_;
	std::string errorMsgWithErrno_;
};

#define DNSFWDEXCEPTION(msg) dnsFwdException{msg}          
#define DNSFWDEXCEPTION_ERR(msg,err) dnsFwdException{msg, err}

} //END OF namespace
#endif

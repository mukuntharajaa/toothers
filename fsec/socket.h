/**
 * Class to represet udp socket to be used in
 * dns forwarded application.
 */
#ifndef _DNS_fwd_H
#define _DNS_FWD_H

#include <memory>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <cstdlib>
#include <unistd.h>
#include <unordered_set>

namespace dns_fwd
{

static const int buffSize = 1472;
//MTU 1500 - 8(UDP header) - 20(IPv4 header)
static const unsigned int rcvBuffSize = 4096000;

//using charArrUniquePtr = std::unique_ptr<char[],void(*)(void *)>;

enum clientmode : bool 
{
	client = true,
	server = false
};

class dnsUdpSocket
{
public:
	dnsUdpSocket(const char *ip, int port, bool mode);
	dnsUdpSocket(const dnsUdpSocket &) = delete;
	dnsUdpSocket( dnsUdpSocket && ) = delete;
	~dnsUdpSocket();

	void createSocket();
	void initializeSocket();
	void setSocketAddress(const char *,int);
	void bindSocket();
	void connectSocket() noexcept;
	void unconnectSocket() noexcept;
	void setRcvBufSize() noexcept;
	void setRcvTimeout();
	void sendData(const char *,uint16_t,int=0);
	void receiveData(char *,uint16_t &);

private:
	int fd_;
	bool clientmode_;
	sockaddr_in addr_;
};

constexpr int sanitizePort(int port);
void checkResponseForFailure( const char *,uint16_t);
void checkDuplicateAndUpdateTransId(const char *req,uint16_t &);
}; //END OF NAMESPACE

#endif

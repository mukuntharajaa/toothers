#include "dnsfwdexception.h"
#include <cstdlib>
#include <cstring>

namespace dns_fwd
{

dnsFwdException::dnsFwdException(const char* error,
                      int errCode) :
        errorMsgPtr_(error),
        errorCode_(errCode),
        errorMsgWithErrno_(error)
{
fillStringWithErrno();
}

void dnsFwdException::fillStringWithErrno() noexcept
{
    if(errorCode_)
    {
        char errbuf[1024] = {'\0'}; //As per man page, GLIBC uses 1024

        /*GNU version of strerror_r works little complicated.
 *        It most of the times returns in char *s
 *        some times in errbuf. Only when errorCode_ is
 *        not in the acceptable range, errbuf will be used
 *        Else it returns in s
 *
 *        */
        char *s = strerror_r(errorCode_,errbuf,1024);
        if( s != NULL )
        {
            errorMsgWithErrno_.append(s,strlen(s));
        }
        else
        {
            errorMsgWithErrno_.append(errbuf,strlen(errbuf));
        }
    }
}

const char* dnsFwdException::what() const noexcept
{
    return errorMsgWithErrno_.c_str();
}

} //END OF namespace

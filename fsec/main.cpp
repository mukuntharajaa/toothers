#include <unordered_set>
#include <iostream>
#include "socket.h"

int main(int argc, char *argv[])
{
    if( argc != 3) 
    {
	std::cerr << "Not enough arguments.\nUsage: " << argv[0] \
		  << " <xx.yy.zz.dd> <port> " << std::endl;
        exit(10);
    }
    #ifdef DBG
	printf("\n\tDebug mode enabled.\n");
    #endif

    try
    {
	/* Create one socket to be in server mode, which accepts incoming connections
 	 * another one to forward the req to actual DNS server and recive response on the same
 	 * and send it back to client on the first created socket
 	 */
	dns_fwd::dnsUdpSocket southBoundSocket("127.0.0.1",9000,dns_fwd::server);
	dns_fwd::dnsUdpSocket northBoundSocket(argv[1],atoi(argv[2]),dns_fwd::client);
	northBoundSocket.setRcvTimeout();
	uint16_t dnsTransId=0;
	char buffer[dns_fwd::buffSize] = {'\0'};
	uint16_t bytes = 0;

	while(1)
	{
		try
		{
		/*1. Wait for incoming connection
 		 *2. Check for duplicate trans_id
		 *3. If all fine, send it to actual DNS server */ 
		memset(buffer,0,sizeof(buffer));
		southBoundSocket.receiveData(buffer,bytes);
		dns_fwd::checkDuplicateAndUpdateTransId(buffer, dnsTransId);
		northBoundSocket.connectSocket();
		northBoundSocket.sendData(buffer,bytes);

		/*4. Wait for response from DNS server
 		 *5. Check whether response is success or failure
		 *6. If all fine, send it back to client */ 
		memset(buffer,0,sizeof(buffer));
		northBoundSocket.receiveData(buffer,bytes);
		dns_fwd::checkResponseForFailure(buffer,dnsTransId);
		southBoundSocket.sendData(buffer,bytes,MSG_CONFIRM);
		northBoundSocket.unconnectSocket();
		dnsTransId=0; //Reset on success case
		}
		catch( const std::exception &e)
		{
			/*Catch and continue */
			northBoundSocket.unconnectSocket();
			std::cerr << e.what() << std::endl;
		}
	}
    }
    catch( const std::exception &e )
    {
	/*Catch and exit, as we can not do anything useful*/
        std::cerr << e.what() << std::endl;
	exit(100);
    }
}

#include "socket.h"
#include <iostream>
#include <sstream>
#include <bitset>
#include "dnsfwdexception.h"
#include "dnspacketheader.h"

namespace dns_fwd
{

#define THROW_SYSTEMCALL_EX(msg) \
    auto err = errno; \
    throw DNSFWDEXCEPTION_ERR(msg,err);

#define THROW_EX(msg) \
	throw DNSFWDEXCEPTION(msg)


/*glibc macro TEMP_FAILURE_RETRY uses glibc extension. If someone uses, uclibc or so,
 * for embedded systems, which not necessarily supply one.
 * So write ourself one */
#define NO_EINTR(stmt) while ((stmt) == -1 && errno == EINTR);


dnsUdpSocket::dnsUdpSocket(const char *ip, int port, bool mode) :
	clientmode_(mode)
{
	createSocket();
	setSocketAddress(ip,port);
	bindSocket();
	setRcvBufSize();
}

void dnsUdpSocket::createSocket()
{
	fd_ = socket(AF_INET, SOCK_DGRAM, 0);
	if ( fd_ < 0 )
	{
		THROW_SYSTEMCALL_EX("socket creation failed:");
        }
	#ifdef DBG
	printf("socket creation success:%d\n",fd_);
	#endif
}

void dnsUdpSocket::setSocketAddress(const char *ip,int port)
{
	memset(&addr_,0,sizeof(addr_));
	addr_.sin_family = AF_INET;
	if( inet_aton(ip, &addr_.sin_addr) == 0 )
	{
		/*No errno set*/
		/*Can not retry*/
		THROW_EX("IPv4 address validation failure.");
	}
        addr_.sin_port = htons(sanitizePort(port));
	#ifdef DBG
	printf("socket address set success:%d\n",port);
	#endif
}

void dnsUdpSocket::bindSocket()
{
	if(!clientmode_)
	{
		int ret{0};
		ret = bind(fd_, (const struct sockaddr *)&addr_,sizeof(addr_));
                if( ret < 0 )
        	{
			THROW_SYSTEMCALL_EX("bind failure:");
        	}
		#ifdef DBG
		printf("bind success\n");
		#endif
	}
}

void dnsUdpSocket::connectSocket() noexcept
{
	if(clientmode_)
	{
		/* Connected-udp socket is neat little trick to 
 		 * catch non-existent server case. But only when
 		 * read operation is done on udp socket, kernel
 		 * will return ECONNREFUSED */
		int ret{0};
		ret = connect(fd_,(const struct sockaddr *)&addr_,sizeof(addr_));
                if( ret < 0 )
		{
		std::cerr << "connect failed : " << strerror(errno) << std::endl;
		/*Its okay if connect fails. Need not stop execution
		 *Anyhow, recvfrom will fail, as timeout is set */
        	}
		#ifdef DBG
		printf("connect success\n");
		#endif
	}
}

void dnsUdpSocket::unconnectSocket() noexcept
{
	if(clientmode_)
	{
		/* Connected-udp socket is neat little trick to 
 		 * catch non-existent server case. But only when
 		 * read operation is done on udp socket, kernel
 		 * will return ECONNREFUSED */
		int ret{0};
		struct sockaddr ad;
		memset(&ad,0,sizeof(struct sockaddr));
		ad.sa_family = AF_UNSPEC;
		ret = connect(fd_,(const struct sockaddr *)&ad,sizeof(ad));
                if( ret < 0 )
		{
		std::cerr << "unconnect failed : " << strerror(errno) << std::endl;
		/*Its okay if connect fails. Need not stop execution
		 *Anyhow, recvfrom will fail, as timeout is set */
        	}
	}
}

void dnsUdpSocket::setRcvBufSize() noexcept
{
	unsigned int nn {0};
        socklen_t m = sizeof(nn);
	int ret {0};
	ret=getsockopt(fd_,SOL_SOCKET,SO_RCVBUF,(void *)&nn, &m);
	if(ret < 0)
	{
		std::cerr << "Error in getting receive buffer : " << strerror(errno) << std::endl;
		return;
	}

        if ( nn < rcvBuffSize)
                nn = rcvBuffSize;

	ret= setsockopt(fd_,SOL_SOCKET,SO_RCVBUF,(const void *)&nn, sizeof(nn));
        if( ret < 0 )
	{
		std::cerr << "Error in setting receive buffer size : " << strerror(errno) << std::endl;
        }
}


void dnsUdpSocket::setRcvTimeout()
{
	struct timeval ts;
        ts.tv_sec = 10;		/* Let us wait 10 second */
        ts.tv_usec = 0;
	int ret {0};
	ret=  setsockopt(fd_,SOL_SOCKET,SO_RCVTIMEO,(const void *)&ts, sizeof(ts));
        if( ret < 0 )
	{
		/* In this failure, we need to exit the process, 
 		 * as without any useful timeout, it could create a deadlock
 		 * situation, as we act as bridge */
		THROW_SYSTEMCALL_EX("Socket timeout failure :");
        }
	#ifdef DBG
	printf("set timeout success\n");
	#endif
}

void dnsUdpSocket::sendData(const char *buffer,uint16_t bytes, int flags)
{
	//MSG_CONFIRM is only for response of a request
	int ret {0};
	NO_EINTR(ret=  sendto(fd_, buffer, bytes,flags, (const struct sockaddr *) &addr_,sizeof(addr_)) );
        if( ret < 0 )
	{
		/* In case of UDP, there is no real send-socket-buffer
 		 * As long as there is space in data-link layer queue,
 		 * sendto will succeed. Even if we are trying to send
 		 * to non-existing server.
 		 * Only if something is wrong at system level, this would
 		 * fail */
		THROW_SYSTEMCALL_EX("sendto failure :");
	}
	#ifdef DBG
	printf("sendto success fd=%d ret=%d\n",fd_,ret);
	#endif
}

void  dnsUdpSocket::receiveData(char *buffer,uint16_t &bytes)
{
   	socklen_t addrlen = sizeof(addr_);
	int ret {0};
	NO_EINTR(ret= recvfrom(fd_, (char *)buffer, buffSize,MSG_WAITALL, (struct sockaddr *) &addr_,&addrlen) );
        if ( ret < 0 )
        {
		/* Either server is down or busy */
		//transId=0;  //this reset, will help client to retry in case recvfrom failure
		//But requirement says to reject all duplicate transid
		THROW_SYSTEMCALL_EX("recvfrom failure :");
        }
	bytes=ret;
	#ifdef DBG
	//printf("recvfrom success fd=%d ret=%d\n",fd_,ret);
	#endif
}

dnsUdpSocket::~dnsUdpSocket()
{
	if( close(fd_) < 0 )
	{
		/*Close should not be retried */
		std::cerr << "close failed: " << strerror(errno) << std::endl;
	}
}

constexpr int sanitizePort(int port)
{
	if( port < 0 || port > 65535 )
	{
		THROW_EX("Invalid port range.");
	}
	return port;
}

void checkResponseForFailure( const char *buffer,uint16_t transId)
{
	const dnsResponse *ptr = reinterpret_cast<const dnsResponse *>(buffer);

	if( transId != ntohs(ptr->trans_id))
		THROW_EX("Mismatch trans_id. Not a valid response.");

	std::bitset<16> bs(ntohs(ptr->respFlags));
	//MSB has request/response code	
	if( !bs.test(15) )
		THROW_EX("Not a response.Something is wrong.");

	//LSB 4 bits has response status
	for ( int i=0; i<4; ++i)
	{
		if( bs.test(i) )
			THROW_EX("Failure response. Not sending back to client.");
	}
	#ifdef DBG
	printf("response check success \n");
	#endif
}

void checkDuplicateAndUpdateTransId(const char *req,uint16_t &transId)
{
	const dnsQuery *ptr = reinterpret_cast<const dnsQuery *>(req);
	auto tid = ntohs(ptr->trans_id);
	if ( tid == transId )
		THROW_EX("Duplicate transId. Not proceeding with query.");

	transId=tid;
	#ifdef DBG
	printf("transid success %u\n",transId);
	#endif
}

/*
void checkDuplicateAndAddTransId(const charArrUniquePtr &req,std::unordered_set<int> &transIdSet)
{
	dnsQuery *ptr = reinterpret_cast<dnsQuery *>(req.get());
	auto tid = ntohs(ptr->trans_id);
	if ( transIdSet.find(tid) != transIdSet.end() )
	{
		THROW_EX("Duplicate transId. Not proceeding with query.");
	}
	auto [first,second] = transIdSet.insert(tid);
	if( second == false )
	{
		std::cerr << "TransId insertion failure.\n";
	}
}*/

}; //END OF NAMESPACE

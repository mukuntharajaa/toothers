#!/bin/bash

NJOBS=$(expr `getconf _NPROCESSORS_ONLN` \* 2)
make clean && make -j$NJOBS && ./dnsfwd 192.168.1.1 53 &
echo -e "\n dns forwarded started. Wait for test scripts success message\n"
./test/simple_test.sh
